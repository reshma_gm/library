import mysql.connector
from datetime import datetime
from datetime import timedelta

mydb = mysql.connector.connect(
    host="localhost",
    user="root",
    passwd="123@password",
    database="library1"
)

mycursor = mydb.cursor()


class Library:
    def __init__(self, book_name=None, no_of_copies=None):

        self.book_name = book_name
        self.no_of_copies = no_of_copies

    @staticmethod
    def add(book_name, copies, year):
        user = (book_name, copies, year)
        try:
            query1 = """SELECT book_title FROM book_details"""
            mycursor.execute(query1)
            result1 = mycursor.fetchall()
            names_list = []
            for row in result1:
                b_name = "".join(row)
                names_list.append(b_name)
            if book_name in names_list:
                mycursor.execute("SELECT no_copies_current FROM book_details WHERE book_title='%s'" % book_name)
                result2 = mycursor.fetchone()
                cur_copies = "".join(result2)
                new_copies = int(copies) + int(cur_copies)
                mycursor.execute("""UPDATE book_details SET no_copies_current = '%s' """ % str(new_copies) + """  WHERE 
                book_title= '%s' """ % book_name)
                mydb.commit()
                print(book_name, " of ", copies, " copies is added to the library")
            else:
                query = """INSERT INTO book_details(book_title, no_copies_current,
                             publication_year) VALUES(%s,%s,%s)"""
                mycursor.execute(query, user)
                mydb.commit()
                print(book_name, " of ", copies, " copies is added to the library")
        except Exception as e:
            print("Unsuccessful!!", e)

    @staticmethod
    def remove(book_name, copies):
        try:
            query1 = """SELECT book_title FROM book_details"""
            mycursor.execute(query1)
            result1 = mycursor.fetchall()
            names_list = []
            for row in result1:
                b_name = "".join(row)
                names_list.append(b_name)
            if book_name in names_list:
                mycursor.execute("SELECT no_copies_current FROM book_details WHERE book_title='%s'" % book_name)
                result2 = mycursor.fetchone()
                cur_copies = "".join(result2)
                new_copies = int(cur_copies) - int(copies)
                mycursor.execute("""UPDATE book_details SET no_copies_current = '%s' """ % str(new_copies) + """  WHERE 
                            book_title= '%s' """ % book_name)
                mydb.commit()
                print(book_name, " of ", copies, " copies is removed")
            else:
                print("Book not found!!")
        except Exception as e:
            print(e)

    def display_available(self):
        query = """ SELECT book_title, no_copies_current, publication_year FROM
                book_details"""
        mycursor.execute(query)
        result = mycursor.fetchall()
        print("BOOK TITLE\t COPIES AVAILABLE\t PUBLICATION YEAR\t ")
        for row in result:
            print(row[0], "  \t\t\t", row[1], "  \t\t\t\t ", row[2], "\t ")

    def display_borrowed(self, username):
        try:
            mycursor.execute("SELECT user_id FROM user_details WHERE user_name='%s'" % username)
            user_id = mycursor.fetchone()
            mycursor.execute("""SELECT B.book_title,BR.borrowed_from_date, BR.Actual_return_date
                FROM book_details B JOIN borrow_details BR
                ON (B.book_isbn = BR.book_isbn) WHERE BR.user_id= '%s'""" % user_id)
            result = mycursor.fetchall()
            print("BOOK TITLE\t\t Borrowed_date\t\t Return_date")
            for row in result:
                print(row[0], "   \t\t ", row[1], "\t\t", row[2])
        except Exception as e:
            print(e)

    def borrow_a_book(self, book_name, username):
        try:
            query1 = """SELECT book_title FROM book_details"""
            mycursor.execute(query1)
            result1 = mycursor.fetchall()
            names_list = []
            for row in result1:
                b_name = "".join(row)
                names_list.append(b_name)
            if book_name in names_list:
                mycursor.execute("SELECT book_isbn FROM book_details WHERE book_title='%s'" % book_name)
                book_isbn = mycursor.fetchone()
                mycursor.execute("SELECT user_id FROM user_details WHERE user_name='%s'" % username)
                user_id = mycursor.fetchone()
                mycursor.execute("""SELECT book_isbn FROM borrow_details WHERE user_id='%s'""" % user_id)
                result2 = mycursor.fetchall()
                books_list = []
                for row in result2:
                    books_list.append(row[0])
                if book_isbn[0] in books_list:
                    print("Book already been issued")
                else:
                    now = datetime.now()
                    date_time = now.strftime("%Y/%m/%d")
                    till = datetime.now() + timedelta(days=15)
                    new_date = till.strftime("%Y/%m/%d")
                    query1 = """INSERT INTO borrow_details(book_isbn,user_id,borrowed_from_date,Actual_return_date) 
                                            VALUES(%s,%s,%s,%s)"""
                    values = (int(book_isbn[0]), int(user_id[0]), date_time, new_date)
                    mycursor.execute(query1, values)
                    mydb.commit()
                    mycursor.execute("SELECT no_copies_current FROM book_details WHERE book_title='%s'" % book_name)
                    result3 = mycursor.fetchone()
                    cur_copies = "".join(result3)
                    new_copies = int(cur_copies) - 1
                    mycursor.execute("""UPDATE book_details SET no_copies_current = '%s' """ % str(new_copies) +
                                     """WHERE book_title= '%s' """ % book_name)
                    mydb.commit()
                    print(book_name, " is issued!!")
            else:
                print(book_name, " is not available!!")
        except Exception as e:
            print(e)

    def return_a_book(self, book_name, username):
        try:
            mycursor.execute("""SELECT user_id FROM user_details WHERE user_name='%s'""" % username)
            user_id = mycursor.fetchone()
            mycursor.execute("""SELECT book_isbn FROM book_details WHERE book_title='%s'""" % book_name)
            book_id = mycursor.fetchone()
            mycursor.execute("""DELETE FROM borrow_details WHERE user_id='%s'""" % int(user_id[0]) + """
            AND book_isbn='%s'""" % int(book_id[0]))
            mydb.commit()
            query1 = """SELECT book_title FROM book_details"""
            mycursor.execute(query1)
            result1 = mycursor.fetchall()
            names_list = []
            for row in result1:
                b_name = "".join(row)
                names_list.append(b_name)
            if book_name in names_list:
                mycursor.execute("SELECT no_copies_current FROM book_details WHERE book_title='%s'" % book_name)
                result2 = mycursor.fetchone()
                cur_copies = "".join(result2)
                new_copies = int(cur_copies) + 1
                mycursor.execute("""UPDATE book_details SET no_copies_current = '%s' """ % str(new_copies) + """  WHERE 
                            book_title= '%s' """ % book_name)
                mydb.commit()
            print("Thanks for returning!!")
        except Exception as e:
            print(e)


class Users:
    def __init__(self, book_name=None, copies=None, name=None, password=None):
        self.book_name = book_name
        self.copies = copies
        self.name = name
        self.password = password

    @staticmethod
    def insert(role):
        if role == "admin":
            book_name = input("Title of the book>> ")
            cur_copies = input("No. of copies currently available>> ")
            year = input("Publication year>> ")
            return book_name, cur_copies, year
        elif role == "user":
            book_name = input("Name of book?\n>")
            return book_name

    @staticmethod
    def insert_details_remove(role):
        if role == "admin":
            book_name = input("Book name>> ")
            copies = input("Number of copies>> ")
            return book_name, copies
        elif role == "user":
            book_name = input("Book name>> ")
            return book_name


    def register(self, email, password, role, username):
        user = (email, password, role, username)
        try:
            query = "INSERT INTO user_details(user_email, password, role, user_name) VALUES(%s,%s,%s,%s)"
            mycursor.execute(query, user)
            mydb.commit()
        except Exception as e:
            print("Unsuccessful Registration!!", e)

    def match(self, email, password):
        try:
            mycursor.execute("SELECT role, password, user_name FROM user_details WHERE user_email='%s' " % email)
            result = mycursor.fetchone()
            if result is None:
                print("Email not found")
            else:
                role = "".join(result[0])
                passwd = "".join(result[1])
                username ="".join(result[2])
                if passwd != password:
                    print("Incorrect password!!")
                else:
                    return role, username
        except Exception as e:
            print("ERROR!!", e)

    def input_details(self):
        email = input("Enter your Email ID>> ")
        password = input("Enter your password>> ")
        role, username = self.match(email, password)
        return role, username

    def register_details(self):
        email = input("Enter your Email ID>> ")
        password = input("Create a password>> ")
        role = input("Enter your role (user/admin)>> ")
        username = input("Enter Your full name>> ")
        return email, password, role, username


def main():
    flag = False

    while not flag:

        print("""
        1.Login
        2.Register""")
        print(">>>>")
        option = int(input())
        if option == 1:
            user_object = Users()
            role, username = user_object.input_details()
            while role == "user":
                print("Welcome ", username, "!!")
                print("""Choose your option>>>>
                1.Borrow a book
                2.Return a book
                3.Display available books
                4.Display borrowed books
                5.Logout""")
                option = int(input(">>>>"))
                if option == 1:
                    lib = Library()
                    book_name = user_object.insert(role)
                    print(book_name)
                    lib.borrow_a_book(book_name, username)
                elif option == 2:
                    lib = Library()
                    lib.return_a_book(user_object.insert_details_remove(role), username)
                elif option == 3:
                    lib = Library()
                    lib.display_available()
                elif option == 4:
                    lib = Library()
                    lib.display_borrowed(username)
                elif option == 5:
                    break
                else:
                    print("Invalid Option")
                    break
            else:
                while role == "admin":
                    print("Welcome ADMIN ", username, ":)")
                    print(""">>>>
                        1.Add books
                        2.Remove books
                        3.Logout""")
                    option = int(input(">>>>"))
                    if option == 1:
                        lib = Library()
                        book_name, cur_copies, year = user_object.insert(role)
                        lib.add(book_name, cur_copies, year)
                    elif option == 2:
                        lib = Library()
                        book_name_remove, copies = user_object.insert_details_remove(role)
                        lib.remove(book_name_remove, copies)
                    elif option == 3:
                        break
                    else:
                        print("Invalid option")
                        break

        else:
            if option == 2:
                user = Users()
                email, password, role, username = user.register_details()
                user.register(email, password, role, username)
            break


main()
