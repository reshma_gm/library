import csv
from sys import exit
from datetime import datetime


lib_dict = {}
user_dict = {}


class Book(object):

    lib_file = r"C:\Users\reshma.gm\PycharmProjects\library\library_books.txt"
    user_file = r"C:\Users\reshma.gm\PycharmProjects\library\user_books.txt"

    def __init__(self, bookname = None, no_of_copies = None):

        self.book_name = bookname
        self.no_of_copies = no_of_copies
        self.lib_dict = {}
        self.user_dict = {}

    def add(self, book_name, no_of_copies):
        with open(self.lib_file,'r') as lb_file:
            for line in lb_file:
                line = line.strip()
                (key, value) = line.split(' ')
                lib_dict[key] = value
        list_names = [bnames for bnames in lib_dict]

        if book_name not in list_names:
            lib_dict[book_name] = no_of_copies
            print(book_name, " is successfully added!!")
        else:
            cur_value = lib_dict.get(book_name)
            new_value = int(cur_value) + int(no_of_copies)
            lib_dict[book_name] = str(new_value)
            print(book_name, " is successfully updated!!")
        self.updating(lib_dict, self.lib_file)

    def remove(self, book_name, copies):
        # del self.lib_dict[book_name]
        with open(self.lib_file) as lb_file:
            for line in lb_file:
                line = line.strip()
                (key, value) = line.split(' ')
                lib_dict[key] = value

        removed_book = book_name
        copies = copies
        count = lib_dict.get(removed_book)
        new_count = int(count) - int(copies)
        updated_dict = {removed_book: str(new_count)}
        lib_dict.update(updated_dict)

        self.updating(lib_dict, self.lib_file)
        print(book_name, " of ", copies, "copies is removed")

    def display_available(self):
        print("BOOK NAME\t No. of copies")
        print("__________________________")
        select_file = open(self.lib_file, 'r')
        for line in select_file.readlines():
            words = line.split(' ')
            if int(words[1]) != 0:
                print(words[0]+"\t\t\t"+words[1])

    def display_borrowed(self):
        print("BOOK NAME\t DATETIME")
        print("__________________________")
        select_file = open(self.user_file, 'r')
        for line in select_file.readlines():
            words = line.split(' ')
            print(words[0] + "\t\t" + (words[1]+words[2]))

    def borrow_a_book(self, book_name):
        with open(self.user_file) as us_file:
            for line in us_file:
                line = line.strip()
                words = line.split(' ')
                value = (words[1] + ' ' + words[2])
                key = words[0]
                user_dict[key] = value
        with open(self.lib_file) as lb_file:
            for line in lb_file:
                line = line.strip()
                (key, value) = line.split(' ')
                lib_dict[key] = value
        if self.book_name in lib_dict:
            if int(lib_dict[book_name]) > 0:
                self.book_name = book_name
                now = datetime.now()
                date_time = now.strftime("%m/%d/%Y, %H:%M:%S")
                list_names = [b_names for b_names in user_dict]
                if book_name not in list_names:
                    user_dict[self.book_name] = date_time
                    print(self.book_name, " is issued!!")
                else:
                    print("You already have a copy of it!! ")
                self.updating(user_dict, self.user_file)

                book_names = [b_names for b_names in lib_dict]
                if self.book_name not in book_names:
                    print("Not available")
                else:
                    cur_value = lib_dict.get(self.book_name)
                    new_value = int(cur_value) - 1
                    lib_dict[self.book_name] = str(new_value)
                self.updating(lib_dict, self.lib_file)
            else:
                print(book_name, " is currently out of stock")

        else:
            print(book_name, " is not available")

    def return_a_book(self, book_name):
        with open(self.user_file, 'r') as us_file:
            for line in us_file:
                line = line.strip()
                words = line.split(' ')
                value = words[1] + ' ' + words[2]
                key = words[0]
                user_dict[key] = value
        with open(self.lib_file) as lb_file:
            for line in lb_file:
                line = line.strip()
                (key, value) = line.split(' ')
                lib_dict[key] = value
        returned_book = book_name
        if returned_book not in user_dict:
            print("Book not been borrowed")
        else:
            del user_dict[returned_book]
            book_names = [b_names for b_names in lib_dict]
            if book_name in book_names:
                cur_value = lib_dict.get(book_name)
                new_value = int(cur_value) + 1
                lib_dict[book_name] = str(new_value)
            self.updating(user_dict, self.user_file)
            self.updating(lib_dict, self.lib_file)
            print("Thanks for returning!!")


    def updating(updated_dict, to_file):
        up_file = open(to_file, 'w')
        for b_name, copies in updated_dict.items():
            up_file.writelines((b_name, " ", copies, "\n"))
        up_file.close()


    def insert(self):
        book_name = input("Name of the book?\n> ")
        copies = input("No. of copies needed?\n> ")
        return book_name, copies
def main():

    book = Book()
    password = "admin"
    flag = False

    while not flag:

        print("""RUN AS>>>>
        USER
        ADMIN""")
        print(">>>>")
        choice = input()
        while choice == "user":
            print("Hello User!!")
            print("""Choose your option>>>>
            1.Borrow a book
            2.Return a book
            3.Display available books
            4.Display borrowed books
            5.EXIT""")
            option = int(input(">>>>"))
            if option == 1:
                book.book_name = input("Name of the book?\n> ")
                book.borrow_a_book(book.book_name)
            elif option == 2:
                book.book_name = input("Bookname:\n> ")
                book.return_a_book(book.book_name)
            elif option == 3:
                book.display_available()
            elif option == 4:
                book.display_borrowed()
            elif option == 5:
                break
            else:
                print("Invalid Option")
                break
        else:
            while choice == password:
                print("Welcome ADMIN!!")
                print(""">>>>
                    1.Add books
                    2.Remove books
                    3.EXIT""")
                option = int(input(">>>>"))
                if option == 1:
                    book.book_name, book.copies = book.insert()
                    book.add(book.book_name, book.copies)
                elif option == 2:
                    book.book_name, book.copies = book.insert()
                    book.remove(book.book_name, book.copies)
                elif option == 3:
                    break
                else:
                    print("Invalid option")
                    break


main()
